u0	INV	this/pro:dem|this is/v:cop|be&3S Investigator_one/n:prop|Investigator_one and/coord|and Investigator_two/n:prop|Investigator_two at/prep|at the/det|the Parent/n:prop|Parent household/n|household with/prep|with Parent/n:prop|Parent and/coord|and her/pro:poss:det|her daughter/n|daughter Child/n:prop|Child ./.|.
u1	INV	child/n|child 's/poss|s birthday/n|birth_day is/v:cop|be&3S April/n:prop|April eight/det:num|eight nineteen/det:num|nineteen eighty/det:num|eighty four/det:num|four ./.|.
u2	INV	it/pro|it 's/v:cop|be&3S July/n:prop|July twenty/det:num|twenty five/det:num|five nineteen/det:num|nineteen eighty/det:num|eighty six/det:num|six ./.|.
u3	MOT	toys/n|toy-PL ?/?|?
u4	INV	started/part|start-PERF ./.|.
u5	IN2	she/pro:sub|she can/mod|can see/v|see what/pro:wh|what 's/v:cop|be&3S in/adv:loc|in there/adv:loc|there ./.|.
u6	INV	lookit/int|look_it what/pro:wh|what 's/v:cop|be&3S in/adv:loc|in here/adv:loc|here ./.|.
u7	CHI	
u8	MOT	the/det|the bear/n|bear ./.|.
u9	IN2	
u10	MOT	tiger/n|tiger ./.|.
u11	MOT	oh/co|oh look/co|look ./.|.
u12	CHI	I/pro:sub|I wan/v|want na/inf|to get/v|get ./.|.
u13	MOT	okay/co|okay ./.|.
u14	CHI	
u15	MOT	what/pro:wh|what is/aux|be&3S it/pro|it ?/?|?
u16	MOT	
u17	MOT	looks/v|look-3S like/prep|like a/det|a porcupine/n|porcupine ./.|.
u18	CHI	
u19	MOT	did/mod|do&PAST n't/neg|not you/pro|you like/v|like that/det|that one/pro:indef|one ?/?|?
u20	MOT	looks/v|look-3S like/prep|like a/det|a puppet/n|puppet ./.|.
u21	MOT	oh/co|oh I/pro:sub|I like/v|like him/pro:obj|him ./.|.
u22	CHI	
u23	CHI	
u24	MOT	hi/co|hi there/adv:loc|there ./.|.
u25	MOT	what/pro:wh|what 's/v:cop|be&3S your/pro:poss:det|your name/n|name ?/?|?
u26	CHI	
u27	MOT	wan/v|want na/inf|to pick/v|pick him/pro:obj|him up/adv:loc|up ?/?|?
u28	MOT	
u29	CHI	look/v|look frog/n|frog ./.|.
u30	CHI	frog/n|frog ./.|.
u31	MOT	frog/n|frog ?/?|?
u32	MOT	no/co|no that/pro:dem|that 's/v:cop|be&3S a/det|a elephant/n|elephant ./.|.
u33	CHI	elephant/n|elephant ./.|.
u34	MOT	not/neg|not too/adv:int|too good/adj|good on/prep|on her/pro:poss:det|her animals/n|animal-PL ./.|.
u35	CHI	see/co|see ?/?|?
u36	CHI	
u37	MOT	it/pro|it 's/v:cop|be&3S the/det|the same/adj|same as/prep|as this/det|this one/pro:indef|one ./.|.
u38	MOT	how/adv:wh|how many/adj|man&dn-Y ?/?|?
u39	CHI	two/det:num|two ./.|.
u40	MOT	
u41	MOT	what/pro:wh|what is/v:cop|be&3S this/pro:dem|this ?/?|?
u42	CHI	
u43	CHI	here/adv:loc|here ./.|.
u44	MOT	
u45	CHI	car/n|car ./.|.
u46	CHI	new/adj|new car/n|car ./.|.
u47	MOT	
u48	CHI	car/n|car ./.|.
u49	MOT	new/adj|new car/n|car ./.|.
u50	MOT	I/pro:sub|I just/adv:int|just got/v|get&PAST a/det|a new/adj|new car/n|car so/adv:int|so every/qn|every car/n|car 's/poss|s a/det|a new/adj|new car/n|car ./.|.
u51	MOT	of/prep|of course/n|course my/pro:poss:det|my car/n|car 's/poss|s two/det:num|two months/n|month-PL old/adj|old but/conj|but it/pro|it 's/v:cop|be&3S still/adv|still pretty/adj|pretty new/adj|new ./.|.
u52	CHI	watch/v|watch ./.|.
u53	CHI	here/adv:loc|here ./.|.
u54	CHI	book/n|book ./.|.
u55	MOT	they/pro:sub|they have/v|have lots_of/qn|lots_of toys/n|toy-PL ./.|.
u56	CHI	book/n|book book/n|book ./.|.
u57	CHI	book/n|book for/prep|for mummie/n|mum-DIM ./.|.
u58	CHI	
u59	CHI	see/v|see book/n|book ./.|.
u60	MOT	oh/co|oh ./.|.
u61	CHI	
u62	CHI	
u63	CHI	
u64	CHI	I/pro:sub|I like/v|like can/mod|can get/v|get toys/n|toy-PL ./.|.
u65	CHI	oh/co|oh ./.|.
u66	CHI	toys/n|toy-PL ./.|.
u67	CHI	oh/co|oh ./.|.
u68	CHI	
u69	CHI	not/neg|not anything/pro:indef|anything look/co|look see/v|see ?/?|?
u70	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u71	MOT	remember/v|remember ?/?|?
u72	MOT	you/pro|you rode/v|ride&PAST on/prep|on one/det:num|one of/prep|of those/pro:dem|those on/prep|on the/det|the merrygoround/n|merry_go_round ./.|.
u73	CHI	look/v|look a/det|a horsey/adj|horsey -/-|-
u74	MOT	right/adj|right ./.|.
u75	CHI	see/co|see I/pro:sub|I gon/part|go-PROG na/inf|to go/v|go a/det|a doggy/n|dog-DIM 'll/mod|will go/v|go round/adv|round around/prep|around ./.|.
u76	MOT	look/v|look at/prep|at the/det|the lion/n|lion ./.|.
u77	CHI	lion/n|lion ./.|.
u78	CHI	see/v|see lion/n|lion ./.|.
u79	MOT	no/co|no that/pro:dem|that 's/v:cop|be&3S a/det|a boar/n|boar ./.|.
u80	CHI	boar/n|boar ./.|.
u81	MOT	test/v|test me/pro:obj|me on/prep|on those/det|those animals/n|animal-PL ./.|.
u82	CHI	look/co|look see/v|see ?/?|?
u83	MOT	you/pro|you know/v|know what/pro:wh|what those/pro:dem|those are/v:cop|be&PRES we/pro:sub|we have/v|have some/qn|some of/prep|of those/det|those up/prep|up the/det|the street/n|street ./.|.
u84	MOT	cow/n|cow ./.|.
u85	CHI	no/co|no ./.|.
u86	MOT	yeah/co|yeah it/pro|it is/v:cop|be&3S ./.|.
u87	MOT	what/pro:wh|what 's/v:cop|be&3S the/det|the cow/n|cow say/co|say ?/?|?
u88	CHI	woof/v|woof woof/on|woof ./.|.
u89	MOT	no/co|no that/pro:dem|that 's/v:cop|be&3S what/pro:wh|what a/det|a dog/n|dog says/v|say-3S ./.|.
u90	MOT	the/det|the cow/n|cow says/v|say-3S moo/on|moo ./.|.
u91	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u92	CHI	
u93	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u94	CHI	doggy/adj|dog&dn-Y ./.|.
u95	MOT	no/co|no -/-|-
u96	CHI	bear/n|bear ./.|.
u97	MOT	right/adj|right ./.|.
u98	CHI	I/pro:sub|I love/v|love him/pro:obj|him ./.|.
u99	MOT	here/adv:loc|here ./.|.
u100	MOT	bears/n|bear-PL are/v:cop|be&PRES her/pro:poss:det|her favorite/n|favorite ./.|.
u101	CHI	look/v|look like/prep|like Gracie/n:prop|Gracie ./.|.
u102	MOT	does/mod|do&3S that/pro:dem|that look/v|look like/prep|like Gracie/n:prop|Gracie ?/?|?
u103	CHI	yeah/co|yeah ./.|.
u104	MOT	I/pro:sub|I do/mod|do n't/neg|not know/v|know ./.|.
u105	MOT	that/pro:dem|that 's/v:cop|be&3S not/neg|not a/det|a doggy/n|dog-DIM though/adv|though ./.|.
u106	CHI	no/co|no oh/co|oh look/v|look like/prep|like Gracie/n:prop|Gracie ?/?|?
u107	MOT	if/conj|if you/pro|you say/v|say so/co|so ./.|.
u108	MOT	that/pro:dem|that 's/v:cop|be&3S who/pro:wh|who the/det|the dog/n|dog is/v:cop|be&3S outside/adv:loc|outside ./.|.
u109	CHI	here/adv:loc|here we/pro:sub|we come/v|come ./.|.
u110	MOT	oh/co|oh I/pro:sub|I 'm/aux|be&1S scared/v|scare-PAST ./.|.
u111	MOT	now/adv|now who/pro:wh|who 's/v:cop|be&3S this/pro:dem|this ?/?|?
u112	MOT	huh/co|huh ?/?|?
u113	CHI	man/n|man ./.|.
u114	MOT	no/co|no ./.|.
u115	MOT	monkey/n|monkey ./.|.
u116	CHI	monkey/n|monkey ./.|.
u117	CHI	the/det|the monkey/n|monkey ./.|.
u118	CHI	here/adv:loc|here we/pro:sub|we come/v|come ./.|.
u119	MOT	oh/co|oh ./.|.
u120	CHI	
u121	CHI	bear/n|bear ./.|.
u122	CHI	look/v|look like/prep|like Gracie/n:prop|Gracie ./.|.
u123	CHI	look/v|look like/prep|like doggy/n|dog-DIM ./.|.
u124	MOT	here/adv:loc|here now/adv|now you/pro|you can/mod|can play/v|play with/prep|with that/pro:dem|that ./.|.
u125	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u126	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u127	MOT	what/pro:wh|what 's/v:cop|be&3S this/det|this one/pro:indef|one ?/?|?
u128	MOT	look/v|look ./.|.
u129	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO ./.|.
u130	CHI	play/v|play ./.|.
u131	MOT	the/det|the dog/n|dog ./.|.
u132	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO ./.|.
u133	CHI	two/det:num|two ./.|.
u134	CHI	right/adj|right ?/?|?
u135	CHI	
u136	CHI	now/adv|now make/v|make a/det|a line/n|line ./.|.
u137	CHI	falling/part|fall-PROG down/adv:loc|down ./.|.
u138	MOT	what/pro:wh|what 's/v:cop|be&3S that/pro:dem|that ?/?|?
u139	CHI	bunny/adj|bun&dn-Y ./.|.
u140	MOT	that/pro:dem|that 's/v:cop|be&3S a/det|a birdie/n|bird-DIM ./.|.
u141	CHI	a/det|a car/n|car ./.|.
u142	CHI	car/n|car ./.|.
u143	MOT	wan/v|want na/inf|to look/v|look in/prep|in the/det|the book/n|book ?/?|?
u144	CHI	yup/co|yup ./.|.
u145	MOT	she/pro:sub|she likes/v|like-3S books/n|book-PL ./.|.
u146	CHI	
u147	MOT	this/pro:dem|this is/v:cop|be&3S a/det|a book/n|book with/prep|with lots_of/qn|lots_of pictures/n|picture-PL ./.|.
u148	MOT	let/v|let 's/pro:obj|us see/v|see what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u149	MOT	you/pro|you remember/v|remember ?/?|?
u150	CHI	yeah/co|yeah ./.|.
u151	MOT	what/pro:wh|what is/aux|be&3S it/pro|it then/adv:tem|then ?/?|?
u152	CHI	cow/n|cow ./.|.
u153	MOT	right/adj|right ./.|.
u154	CHI	cow/n|cow ./.|.
u155	MOT	what/pro:wh|what 's/v:cop|be&3S the/det|the cow/n|cow say/co|say ?/?|?
u156	CHI	woof/v|woof woof/on|woof ./.|.
u157	MOT	no/co|no -/-|-
u158	MOT	he/pro:sub|he says/v|say-3S moo/on|moo -/-|-
u159	CHI	no/co|no ./.|.
u160	MOT	you/pro|you knew/v|know&PAST them/pro:obj|them better/adj|good&CP when/conj|when you/pro|you were/v:cop|be&PAST littler/adj|little-CP ./.|.
u161	CHI	see/co|see ?/?|?
u162	CHI	I/pro:sub|I 'm/aux|be&1S put/v|put&ZERO him/pro:obj|him right/adv|right here/adv:loc|here ./.|.
u163	CHI	I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to play/v|play ./.|.
u164	CHI	no/co|no Mommy/n:prop|Mommy ./.|.
u165	CHI	wan/v|want na/inf|to put/v|put&ZERO it/pro|it right/adv|right here/adv:loc|here ./.|.
u166	CHI	all/adv:int|all gone/part|go&PERF ./.|.
u167	CHI	all/adv:int|all gone/part|go&PERF ./.|.
u168	CHI	see/v|see all/adv:int|all gone/part|go&PERF ./.|.
u169	MOT	where/adv:wh|where did/mod|do&PAST they/pro:sub|they go/v|go ?/?|?
u170	CHI	right/adv|right here/adv:loc|here ./.|.
u171	MOT	oh/co|oh ./.|.
u172	CHI	I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to get/v|get ./.|.
u173	MOT	you/pro|you look/v|look silly/adj|silly with/prep|with your/pro:poss:det|your hat/n|hat on/adv|on ./.|.
u174	CHI	
u175	MOT	thanks/co|thanks ./.|.
u176	CHI	here/adv:loc|here ./.|.
u177	CHI	let/v|let&ZERO me/pro:obj|me get/v|get ./.|.
u178	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO him/pro:obj|him up/adv:loc|up ./.|.
u179	CHI	here/adv:loc|here we/pro:sub|we go/v|go ./.|.
u180	MOT	can/mod|can you/pro|you make/v|make him/pro:obj|him fly/v|fly ?/?|?
u181	MOT	it/pro|it 's/v:cop|be&3S a/det|a birdie/n|bird-DIM ./.|.
u182	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO him/pro:obj|him ./.|.
u183	CHI	let/v|let&ZERO me/pro:obj|me play/v|play ./.|.
u184	MOT	
u185	CHI	no/co|no ./.|.
u186	MOT	no/co|no ?/?|?
u187	MOT	you/pro|you do/mod|do n't/neg|not want/v|want any/qn|any ?/?|?
u188	CHI	here/adv:loc|here ./.|.
u189	MOT	but/conj|but you/pro|you ca/mod|can n't/neg|not eat/v|eat those/pro:dem|those ./.|.
u190	CHI	no/co|no ./.|.
u191	CHI	
u192	CHI	here/adv:loc|here two/det:num|two right/adv|right ?/?|?
u193	CHI	here/adv:loc|here two/det:num|two mummie/n|mum-DIM ?/?|?
u194	CHI	bear/n|bear ./.|.
u195	CHI	I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to get/v|get another/qn|another book/n|book ./.|.
u196	MOT	I/pro:sub|I think/v|think all/qn|all your/pro:poss:det|your books/n|book-PL are/v:cop|be&PRES out/adv:loc|out here/adv:loc|here ./.|.
u197	CHI	no/co|no ./.|.
u198	MOT	no/co|no ?/?|?
u199	MOT	okay/co|okay ./.|.
u200	CHI	no/co|no right/adv|right here/adv:loc|here ./.|.
u201	MOT	here/adv:loc|here ./.|.
u202	CHI	I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to get/v|get ./.|.
u203	MOT	wan/v|want na/inf|to look/v|look at/prep|at it/pro|it ?/?|?
u204	CHI	yeah/co|yeah ./.|.
u205	MOT	it/pro|it should/mod|should be/v:cop|be good/adj|good ./.|.
u206	CHI	
u207	MOT	yeah/co|yeah ./.|.
u208	CHI	see/co|see ?/?|?
u209	CHI	see/v|see bunny/n|bunny ?/?|?
u210	MOT	where/adv:wh|where 's/v:cop|be&3S the/det|the bunny/n|bunny is/v:cop|be&3S he/pro:sub|he hiding/part|hide-PROG ?/?|?
u211	MOT	can/mod|can you/pro|you find/v|find him/pro:obj|him ?/?|?
u212	MOT	yes/co|yes ./.|.
u213	MOT	there/adv:loc|there he/pro:sub|he is/v:cop|be&3S ./.|.
u214	MOT	where/adv:wh|where is/v:cop|be&3S he/pro:sub|he ?/?|?
u215	CHI	
u216	CHI	right/adv|right here/adv:loc|here ./.|.
u217	MOT	where/adv:wh|where 's/v:cop|be&3S the/det|the bunny/n|bunny hiding/part|hide-PROG ?/?|?
u218	CHI	do/mod|do n't/neg|not touch/v|touch ./.|.
u219	CHI	
u220	CHI	up/adv:loc|up ./.|.
u221	CHI	I/pro:sub|I got/v|get&PAST a/det|a car/n|car ./.|.
u222	CHI	I/pro:sub|I got/v|get&PAST I/pro:sub|I got/v|get&PAST a/det|a car/n|car ./.|.
u223	IN2	that/pro:dem|that falls/n|fall-PL off/adv|off all/qn|all the/det|the time/n|time ./.|.
u224	CHI	I/pro:sub|I got/v|get&PAST a/det|a car/n|car ./.|.
u225	INV	kind/n|kind of/prep|of tricky/adj|trick&dn-Y to/inf|to get/v|get it/pro|it back/adv:loc|back on/adv|on too/post|too ./.|.
u226	CHI	I/pro:sub|I did/v|do&PAST ./.|.
u227	MOT	you/pro|you did/mod|do&PAST it/pro|it ?/?|?
u228	CHI	yeah/co|yeah ./.|.
u229	MOT	yeah/co|yeah well/co|well sort/v|sort of/prep|of ./.|.
u230	CHI	I/pro:sub|I wan/v|want na/inf|to do/v|do I/pro:sub|I wan/v|want na/inf|to fix/v|fix ./.|.
u231	CHI	I/pro:sub|I wan/v|want na/inf|to fix/v|fix ./.|.
u232	CHI	let/v|let&ZERO me/pro:obj|me fix/v|fix ./.|.
u233	CHI	I/pro:sub|I fixed/v|fix-PAST ./.|.
u234	MOT	you/pro|you fixed/v|fix-PAST it/pro|it ?/?|?
u235	CHI	see/co|see ?/?|?
u236	CHI	
u237	CHI	see/co|see I/pro:sub|I did/mod|do&PAST it/pro|it ./.|.
u238	MOT	
u239	CHI	
u240	CHI	a/det|a door/n|door of/prep|of my/pro:poss:det|my car/n|car ./.|.
u241	MOT	that/pro:dem|that your/pro:poss:det|your car/n|car ?/?|?
u242	CHI	I/pro:sub|I show/v|show you/pro|you ./.|.
u243	MOT	need/v|need help/v|help ?/?|?
u244	CHI	
u245	CHI	maybe/adv|maybe it/pro|it goes/v|go-3S this/det|this way/n|way ./.|.
u246	CHI	see/co|see I/pro:sub|I did/v|do&PAST ./.|.
u247	CHI	
u248	CHI	I/pro:sub|I need/v|need help/v|help ./.|.
u249	CHI	it/pro|it look/v|look Gammy/n:prop|Gammy car/n|car ?/?|?
u250	MOT	it/pro|it looks/v|look-3S like/prep|like Grammy/n:prop|Grammy 's/poss|s car/n|car she/pro:sub|she has/v|have&3S a/det|a station/n|station wagon/n|wagon ./.|.
u251	MOT	it/pro|it 's/v:cop|be&3S green/adj|green too/adv:int|too ./.|.
u252	CHI	I/pro:sub|I like/v|like my/pro:poss:det|my I/pro:sub|I like/v|like my/pro:poss:det|my new/adj|new car/n|car ./.|.
u253	MOT	your/pro:poss:det|your new/adj|new car/n|car ?/?|?
u254	CHI	I/pro:sub|I like/v|like Gammy/n:prop|Gammy car/n|car ./.|.
u255	CHI	
u256	CHI	let/v|let&ZERO me/pro:obj|me fix/v|fix ./.|.
u257	CHI	
u258	CHI	I/pro:sub|I wan/v|want na/inf|to fix/v|fix ./.|.
u259	CHI	I/pro:sub|I need/v|need help/v|help ./.|.
u260	MOT	do/mod|do you/pro|you need/v|need help/v|help ?/?|?
u261	MOT	
u262	CHI	
u263	MOT	who/pro:wh|who 's/aux|be&3S him/pro:obj|him ?/?|?
u264	MOT	remember/v|remember him/pro:obj|him ?/?|?
u265	CHI	yeah/co|yeah ./.|.
u266	MOT	we/pro:sub|we read/v|read&ZERO a/det|a book/n|book with/prep|with him/pro:obj|him ./.|.
u267	MOT	gorilla/n|gorilla ./.|.
u268	MOT	you/pro|you say/v|say it/pro|it ./.|.
u269	CHI	
u270	MOT	say/v|say gorilla/n|gorilla ./.|.
u271	CHI	gorilla/n|gorilla ./.|.
u272	MOT	almost/adv|almost ./.|.
u273	CHI	here/adv:loc|here we/pro:sub|we come/v|come ./.|.
u274	MOT	that/det|that one/pro:indef|one was/v:cop|be&PAST&13S kind/adj|kind of/prep|of hard/adj|hard ./.|.
u275	CHI	here/adv:loc|here we/pro:sub|we come/v|come ./.|.
u276	MOT	oh/co|oh ./.|.
u277	CHI	here/adv:loc|here we/pro:sub|we come/v|come ./.|.
u278	MOT	oops/int|oops ./.|.
u279	MOT	who/pro:wh|who 's/v:cop|be&3S that/pro:dem|that a/det|a tiger/n|tiger ?/?|?
u280	CHI	nope/co|nope ./.|.
u281	CHI	here/adv:loc|here we/pro:sub|we come/v|come ./.|.
u282	MOT	
u283	CHI	
u284	CHI	
u285	CHI	oh/co|oh ./.|.
u286	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u287	CHI	a/det|a man/n|man ./.|.
u288	MOT	no/co|no ./.|.
u289	MOT	I/pro:sub|I just/adv:int|just told/v|tell&PAST you/pro|you ./.|.
u290	MOT	what/pro:wh|what 's/aux|be&3S it/pro|it look/v|look like/prep|like ?/?|?
u291	MOT	
u292	MOT	gorilla/n|gorilla ./.|.
u293	CHI	gorilla/n|gorilla ./.|.
u294	MOT	closer/n|close&dv-AGT ./.|.
u295	MOT	gorilla/n|gorilla ./.|.
u296	CHI	no/co|no ./.|.
u297	MOT	remember/v|remember yesterday/adv:tem|yesterday Rachel/n:prop|Rachel 's/poss|s koala/n|koala bear/n|bear ./.|.
u298	MOT	can/mod|can you/pro|you say/v|say that/pro:dem|that ?/?|?
u299	CHI	no/co|no ./.|.
u300	MOT	you/pro|you got/v|get&PAST close/adj|close yesterday/adv:tem|yesterday ./.|.
u301	CHI	all/adv:int|all gone/part|go&PERF ./.|.
u302	MOT	can/mod|can you/pro|you say/v|say koala/n|koala ?/?|?
u303	CHI	no/co|no ./.|.
u304	MOT	yes/co|yes you/pro|you can/mod|can ./.|.
u305	CHI	koala/n|koala ./.|.
u306	MOT	right/adj|right ./.|.
u307	MOT	who/pro:wh|who 's/v:cop|be&3S this/pro:dem|this ?/?|?
u308	CHI	horsey/adj|horsey ./.|.
u309	MOT	no/co|no ./.|.
u310	MOT	that/pro:dem|that 's/v:cop|be&3S a/det|a doggy/n|dog-DIM ./.|.
u311	MOT	what/pro:wh|what 's/v:cop|be&3S this/det|this one/pro:indef|one ?/?|?
u312	CHI	owl/n|owl ./.|.
u313	MOT	an/det|a owl/n|owl ?/?|?
u314	CHI	
u315	CHI	elephant/n|elephant ./.|.
u316	MOT	elephant/n|elephant ./.|.
u317	MOT	right/adj|right ./.|.
u318	CHI	I/pro:sub|I love/v|love elephant/n|elephant ./.|.
u319	MOT	you/pro|you love/v|love him/pro:obj|him ?/?|?
u320	MOT	he/pro:sub|he loves/v|love-3S you/pro|you too/post|too ./.|.
u321	CHI	Gracie/n:prop|Gracie ./.|.
u322	MOT	is/v:cop|be&3S that/pro:dem|that Gracie/n:prop|Gracie ?/?|?
u323	CHI	yeah/co|yeah ./.|.
u324	MOT	Gracie/n:prop|Gracie 's/aux|be&3S not/neg|not an/det|a elephant/n|elephant ./.|.
u325	CHI	that/pro:dem|that Gracie/n:prop|Gracie ./.|.
u326	MOT	what/pro:wh|what 's/v:cop|be&3S this/det|this one/pro:indef|one ?/?|?
u327	CHI	let/v|let&ZERO me/pro:obj|me do/v|do ./.|.
u328	MOT	what/pro:wh|what 's/v:cop|be&3S his/pro:poss:det|his name/n|name ?/?|?
u329	CHI	two/det:num|two ./.|.
u330	MOT	two/det:num|two ./.|.
u331	CHI	
u332	MOT	How/n:prop|How come/v|come you/pro|you do/mod|do n't/neg|not like/v|like this/pro:dem|this ?/?|?
u333	MOT	right/adv|right there/adv:loc|there ./.|.
u334	MOT	ouch/co|ouch ./.|.
u335	MOT	ouch/co|ouch ./.|.
u336	MOT	ouch/co|ouch ./.|.
u337	MOT	
u338	MOT	got/v|get&PAST your/pro:poss:det|your nose/n|nose ./.|.
u339	CHI	
u340	MOT	got/v|get&PAST your/pro:poss:det|your nose/n|nose ./.|.
u341	MOT	you/pro|you want/v|want it/pro|it ?/?|?
u342	CHI	no/co|no ./.|.
u343	MOT	you/pro|you do/mod|do n't/neg|not want/v|want your/pro:poss:det|your nose/n|nose ?/?|?
u344	MOT	gon/part|go-PROG na/inf|to throw/v|throw it/pro|it away/adv|away ./.|.
u345	CHI	
u346	CHI	I/pro:sub|I want/v|want it/pro|it back/adv:loc|back ./.|.
u347	MOT	you/pro|you want/v|want it/pro|it back/adv:loc|back ?/?|?
u348	MOT	okay/co|okay ./.|.
u349	MOT	where/adv:wh|where do/mod|do you/pro|you want/v|want it/pro|it ?/?|?
u350	CHI	right/adv|right here/adv:loc|here ./.|.
u351	MOT	okay/co|okay ./.|.
u352	MOT	can/mod|can he/pro:sub|he take/v|take it/pro|it again/adv|again ?/?|?
u353	MOT	he/pro:sub|he 's/aux|be&3S got/part|get&PERF your/pro:poss:det|your hand/n|hand now/adv|now ./.|.
u354	MOT	gon/part|go-PROG na/inf|to throw/v|throw your/pro:poss:det|your hand/n|hand away/adv|away ?/?|?
u355	CHI	I/pro:sub|I want/v|want it/pro|it back/adv:loc|back ./.|.
u356	MOT	you/pro|you want/v|want it/pro|it back/adv:loc|back ?/?|?
u357	MOT	where/adv:wh|where ?/?|?
u358	CHI	
u359	MOT	how/adv:wh|how about/prep|about your/pro:poss:det|your toes/n|toe-PL ./.|.
u360	MOT	throw/v|throw them/pro:obj|them away/adv|away ?/?|?
u361	CHI	I/pro:sub|I want/v|want them/pro:obj|them back/adv:loc|back nose/n|nose ./.|.
u362	MOT	you/pro|you want/v|want your/pro:poss:det|your nose/n|nose ?/?|?
u363	CHI	I/pro:sub|I want/v|want them/pro:obj|them ./.|.
u364	INV	well/co|well I/pro:sub|I guess/n|guess she/pro:sub|she 'll/mod|will never/adv|never get/v|get her/pro:poss:det|her toes/n|toe-PL back/adv:loc|back ./.|.
u365	CHI	where/adv:wh|where my/pro:poss:det|my duck/n|duck ?/?|?
u366	CHI	duck/n|duck ./.|.
u367	CHI	fix/v|fix ./.|.
u368	CHI	right/adj|right ?/?|?
u369	CHI	
u370	MOT	we/pro:sub|we 've/aux|have been/v:cop|be&PERF trying/n:gerund|try-PROG ./.|.
u371	CHI	I/pro:sub|I fix/v|fix ./.|.
u372	CHI	see/co|see ?/?|?
u373	MOT	did/mod|do&PAST you/pro|you do/v|do a/det|a good/adj|good job/n|job ?/?|?
u374	CHI	yeah/co|yeah ./.|.
u375	MOT	
u376	CHI	I/pro:sub|I like/v|like go/v|go in/prep|in Gammy/n:prop|Gammy car/n|car ./.|.
u377	CHI	I/pro:sub|I like/v|like Gammy/n:prop|Gammy car/n|car ./.|.
u378	CHI	she/pro:sub|she she/pro:sub|she gon/part|go-PROG na/inf|to buy/v|buy a/det|a car/n|car ?/?|?
u379	MOT	where/adv:wh|where 's/v:cop|be&3S Grandpa/n:prop|Grandpa is/v:cop|be&3S he/pro:sub|he in/adv:loc|in there/adv:loc|there ?/?|?
u380	CHI	yeah/co|yeah ./.|.
u381	MOT	he/pro:sub|he is/v:cop|be&3S ?/?|?
u382	MOT	where/adv:wh|where 's/v:cop|be&3S Grandpa/n:prop|Grandpa ?/?|?
u383	CHI	new/adj|new car/n|car ./.|.
u384	CHI	
u385	CHI	I/pro:sub|I like/v|like Gammy/n:prop|Gammy car/n|car ./.|.
u386	CHI	me/pro:obj|me take/v|take out/n|out ./.|.
u387	CHI	oh/co|oh ./.|.
u388	MOT	there/adv:loc|there ./.|.
u389	CHI	where/adv:wh|where it/pro|it go/v|go ?/?|?
u390	CHI	right/adv|right here/adv:loc|here ?/?|?
u391	MOT	
u392	CHI	like/v|like Gammy/n:prop|Gammy car/n|car ./.|.
u393	CHI	make/v|make it/pro|it go/v|go ./.|.
u394	CHI	right/adv|right here/adv:loc|here ./.|.
u395	CHI	there/adv:loc|there go/v|go Gammy/n:prop|Gammy car/n|car ./.|.
u396	CHI	this/pro:dem|this is/v:cop|be&3S like/prep|like Grandpa/n:prop|Grandpa car/n|car ./.|.
u397	MOT	whose/det:wh|whose car/n|car is/v:cop|be&3S it/pro|it ?/?|?
u398	CHI	mine/pro:poss|mine and/coord|and Grandpa/n:prop|Grandpa ./.|.
u399	MOT	your/pro:poss:det|your car/n|car ?/?|?
u400	CHI	no/co|no ./.|.
u401	MOT	my/pro:poss:det|my car/n|car ./.|.
u402	CHI	my/pro:poss:det|my car/n|car ./.|.
u403	MOT	my/pro:poss:det|my car/n|car ./.|.
u404	CHI	my/pro:poss:det|my car/n|car ./.|.
u405	MOT	my/pro:poss:det|my car/n|car ./.|.
u406	CHI	here/adv:loc|here ./.|.
u407	MOT	oh/co|oh this/pro:dem|this is/v:cop|be&3S cute/adj|cute ./.|.
u408	CHI	I/pro:sub|I playing/part|play-PROG with/prep|with ./.|.
u409	MOT	
u410	MOT	who/pro:wh|who 's/v:cop|be&3S this/pro:dem|this ?/?|?
u411	MOT	a/det|a chicken/n|chicken ./.|.
u412	MOT	
u413	CHI	I/pro:sub|I want/v|want the/det|the ./.|.
u414	CHI	I/pro:sub|I do/mod|do n't/neg|not want/v|want him/pro:obj|him ./.|.
u415	MOT	you/pro|you do/mod|do n't/neg|not like/v|like him/pro:obj|him ?/?|?
u416	CHI	yeah/co|yeah ./.|.
u417	MOT	why/adv:wh|why not/neg|not ?/?|?
u418	CHI	I/pro:sub|I do/mod|do n't/neg|not like/v|like chicken/n|chicken ./.|.
u419	MOT	oh/co|oh ./.|.
u420	MOT	how/adv:wh|how about/prep|about the/det|the rooster/n|rooster ?/?|?
u421	CHI	yeah/co|yeah ./.|.
u422	MOT	you/pro|you like/v|like the/det|the rooster/n|rooster ?/?|?
u423	CHI	yeah/co|yeah ./.|.
u424	INV	want/v|want the/det|the rooster/n|rooster ?/?|?
u425	MOT	why/adv:wh|why do/mod|do n't/neg|not you/pro|you like/v|like the/det|the chicken/n|chicken ?/?|?
u426	CHI	I/pro:sub|I do/mod|do n't/neg|not know/v|know ./.|.
u427	INV	you/pro|you can/mod|can make/v|make the/det|the rooster/n|rooster talk/n|talk to/prep|to the/det|the chicken/n|chicken ./.|.
u428	MOT	can/mod|can we/pro:sub|we hear/v|hear him/pro:obj|him talk/v|talk ?/?|?
u429	CHI	see/co|see ?/?|?
u430	MOT	hi/co|hi rooster/n|rooster ./.|.
u431	MOT	can/mod|can you/pro|you do/v|do it/pro|it ?/?|?
u432	CHI	no/co|no ./.|.
u433	MOT	yes/co|yes you/pro|you can/mod|can ./.|.
u434	CHI	no/co|no ./.|.
u435	MOT	put/v|put&ZERO it/pro|it on/prep|on your/pro:poss:det|your hand/n|hand ?/?|?
u436	MOT	hi/co|hi rooster/n|rooster ./.|.
u437	CHI	chicken/n|chicken ./.|.
u438	MOT	
u439	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO him/pro:obj|him here/adv:loc|here ./.|.
u440	CHI	like/v|like chicken/n|chicken ./.|.
u441	INV	
u442	CHI	
u443	MOT	
u444	CHI	I/pro:sub|I wan/v|want na/inf|to put/v|put&ZERO a/det|a book/n|book ./.|.
u445	CHI	
u446	CHI	my/pro:poss:det|my books/n|book-PL ./.|.
u447	CHI	I/pro:sub|I put/v|put&ZERO away/adv|away ./.|.
u448	MOT	see/v|see if/conj|if you/pro|you remember/v|remember this/det|this one/pro:indef|one ./.|.
u449	MOT	what/pro:wh|what 's/v:cop|be&3S this/pro:dem|this ?/?|?
u450	CHI	house/n|house ./.|.
u451	MOT	no/co|no but/conj|but what/pro:wh|what kind/n|kind of/prep|of a/det|a house/n|house ?/?|?
u452	CHI	
u453	MOT	no/co|no -/-|-
u454	CHI	I/pro:sub|I wan/v|want na/inf|to get/v|get another/qn|another book/n|book ./.|.
u455	MOT	it/pro|it now/adv|now she/pro:sub|she says/v|say-3S two/det:num|two ./.|.
u456	MOT	is/v:cop|be&3S it/pro|it a/det|a gazebo/n|gazebo ?/?|?
u457	CHI	no/co|no ./.|.
u458	MOT	no/co|no ?/?|?
u459	CHI	again/adv|again ./.|.
u460	CHI	see/co|see ?/?|?
u461	CHI	see/v|see open/v|open ./.|.
u462	CHI	they/pro:sub|they go/v|go in/adv:loc|in right/adv|right ?/?|?
u463	MOT	is/v:cop|be&3S that/pro:dem|that the/det|the right/adj|right place/n|place ?/?|?
u464	CHI	yeah/co|yeah ./.|.
u465	CHI	all/adv:int|all gone/part|go&PERF ./.|.
u466	CHI	I/pro:sub|I I/pro:sub|I wan/v|want na/inf|to get/v|get them/pro:obj|them out/prep|out more/qn|more ./.|.
u467	CHI	put/v|put&ZERO it/pro|it right/adv|right here/adv:loc|here ?/?|?
u468	MOT	yeah/co|yeah ./.|.
u469	CHI	put/v|put&ZERO it/pro|it right/adv|right here/adv:loc|here ?/?|?
u470	CHI	put/v|put&ZERO it/pro|it right/adv|right here/adv:loc|here ?/?|?
u471	MOT	yeah/co|yeah ./.|.
u472	CHI	all/adv:int|all gone/part|go&PERF ./.|.
u473	MOT	but/conj|but you/pro|you have_to/mod:aux|have_to push/v|push the/det|the other/qn|other button/n|button too/post|too ./.|.
u474	MOT	see/co|see it/pro|it 's/aux|be&3S stuck/part|stick&PERF ./.|.
u475	CHI	I/pro:sub|I push/v|push ./.|.
u476	CHI	that/pro:dem|that push/n|push right/adv|right ?/?|?
u477	CHI	they/pro:sub|they go/v|go on/adv:loc|on ?/?|?
u478	MOT	what/pro:wh|what color/n|color is/v:cop|be&3S this/det|this one/pro:indef|one ?/?|?
u479	CHI	two/det:num|two ./.|.
u480	MOT	no/co|no what/pro:wh|what color/n|color is/v:cop|be&3S it/pro|it ?/?|?
u481	CHI	this/pro:dem|this ./.|.
u482	MOT	it/pro|it 's/v:cop|be&3S the/det|the same/adj|same color/n|color as/prep|as Mummy/n:prop|Mummy 's/poss|s car/n|car ./.|.
u483	CHI	two/det:num|two ./.|.
u484	CHI	
u485	MOT	push/v|push this/det|this button/n|button ./.|.
u486	CHI	I/pro:sub|I wan/v|want na/inf|to I/pro:sub|I can/mod|can I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to push/v|push this/det|this one/pro:indef|one alright/co|alright ?/?|?
u487	CHI	I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to push/v|push it/pro|it in/adv:loc|in ./.|.
u488	MOT	then/adv:tem|then push/v|push the/det|the orange/adj|orange one/pro:indef|one in/adv:loc|in ./.|.
u489	MOT	no/co|no the/det|the orange/adj|orange one/pro:indef|one ./.|.
u490	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO ./.|.
u491	MOT	
u492	CHI	I/pro:sub|I ca/mod|can n't/neg|not ./.|.
u493	CHI	I/pro:sub|I ca/mod|can n't/neg|not ./.|.
u494	CHI	wan/v|want na/inf|to put/v|put&ZERO more/qn|more ./.|.
u495	CHI	I/pro:sub|I 'm/aux|be&1S gon/part|go-PROG na/inf|to put/v|put&ZERO more/qn|more again/adv|again ./.|.
u496	CHI	here/adv:loc|here red/n|red one/pro:indef|one -/-|-
u497	CHI	right/adv|right down/adv:loc|down there/adv:loc|there ./.|.
u498	MOT	what/pro:wh|what color/n|color 's/v:cop|be&3S that/det|that one/pro:indef|one ?/?|?
u499	CHI	two/det:num|two ./.|.
u500	MOT	it/pro|it 's/v:cop|be&3S red/adj|red ./.|.
u501	CHI	in/adv:loc|in in/adv:loc|in ./.|.
u502	CHI	look/v|look ./.|.
u503	MOT	what/pro:wh|what 's/v:cop|be&3S your/pro:poss:det|your name/n|name ?/?|?
u504	CHI	two/det:num|two ./.|.
u505	MOT	she/pro:sub|she tells/v|tell-3S everyone/pro:indef|everyone her/pro:poss:det|her name/n|name is/v:cop|be&3S two/det:num|two ./.|.
u506	CHI	there/adv:loc|there they/pro:sub|they in/adv:loc|in ./.|.
u507	MOT	what/pro:wh|what 's/v:cop|be&3S your/pro:poss:det|your name/n|name ?/?|?
u508	MOT	what/pro:wh|what 's/v:cop|be&3S your/pro:poss:det|your name/n|name ?/?|?
u509	CHI	Lastname/n:prop|Lastname -/-|-
u510	MOT	Lastname/n:prop|Lastname right/adv|right but/conj|but what/pro:wh|what 's/v:cop|be&3S your/pro:poss:det|your first/adj|first name/n|name ?/?|?
u511	CHI	Lastname/n:prop|Lastname ./.|.
u512	MOT	right/co|right you/pro|you never/adv|never answer/v|answer that/det|that one/pro:indef|one when/conj|when I/pro:sub|I ask/v|ask ./.|.
u513	MOT	what/pro:wh|what 's/v:cop|be&3S your/pro:poss:det|your other/qn|other name/n|name ?/?|?
u514	CHI	two/det:num|two ./.|.
u515	MOT	oh/co|oh ./.|.
u516	MOT	is/v:cop|be&3S it/pro|it Child/n:prop|Child ?/?|?
u517	CHI	no/co|no ./.|.
u518	MOT	no/co|no ?/?|?
u519	MOT	then/adv:tem|then I/pro:sub|I do/mod|do n't/neg|not know/v|know what/pro:wh|what it/pro|it is/v:cop|be&3S ./.|.
u520	MOT	where/adv:wh|where do/mod|do you/pro|you live/adj|live ?/?|?
u521	CHI	Place/n:prop|Place ./.|.
u522	MOT	Place/n:prop|Place right/adv|right ./.|.
u523	MOT	nobody/pro:indef|nobody 'll/mod|will understand/v|understand you/pro|you ./.|.
u524	MOT	see/v|see at/prep|at least/n|least she/pro:sub|she knows/v|know-3S what/pro:wh|what town/n|town she/pro:sub|she 'll/mod|will get/v|get to/prep|to the/det|the right/adj|right town/n|town if/conj|if she/pro:sub|she gets/v|get-3S lost/adj|lost ./.|.
u525	MOT	and/coord|and he/pro:sub|he says/v|say-3S -/-|-
u526	MOT	yeah/co|yeah no/co|no one/pro:indef|one 's/aux|be&3S going/part|go-PROG to/inf|to understand/v|understand ./.|.
u527	MOT	
u528	INV	well/co|well that/pro:dem|that 's/v:cop|be&3S close/adj|close ./.|.
u529	MOT	right/co|right we/pro:sub|we 'll/mod|will work/v|work on/prep|on it/pro|it ./.|.
u530	MOT	say/co|say Place/n:prop|Place ./.|.
u531	CHI	no/co|no ./.|.
u532	MOT	no/co|no ?/?|?
u533	MOT	do/mod|do n't/neg|not eat/v|eat that/pro:dem|that ./.|.
u534	MOT	do/mod|do n't/neg|not eat/v|eat that/pro:dem|that ./.|.
u535	MOT	that/pro:dem|that 's/v:cop|be&3S yucky/adj|yuck&dn-Y ./.|.
u536	CHI	no/co|no -/-|-
u537	MOT	hey/co|hey -/-|-
u538	CHI	let/v|let&ZERO me/pro:obj|me get/v|get ./.|.
u539	MOT	where/adv:wh|where 're/aux|be&PRES you/pro|you going/part|go-PROG ?/?|?
u540	MOT	what/pro:wh|what are/aux|be&PRES you/pro|you gon/part|go-PROG na/inf|to get/v|get ?/?|?
u541	MOT	you/pro|you getting/part|get-PROG something/pro:indef|something ?/?|?
u542	MOT	you/pro|you get/v|get something/pro:indef|something ?/?|?
u543	MOT	
u544	MOT	why/adv:wh|why 'd/mod|genmod you/pro|you close/v|close the/det|the door/n|door ?/?|?
u545	CHI	no/co|no ./.|.
u546	MOT	no/co|no ?/?|?
u547	CHI	I/pro:sub|I close/v|close the/det|the door/n|door ./.|.
u548	MOT	why/adv:wh|why ?/?|?
u549	CHI	I/pro:sub|I do/mod|do n't/neg|not know/v|know ./.|.
u550	CHI	I/pro:sub|I wan/v|want na/inf|to put/v|put&ZERO ./.|.
u551	MOT	is/v:cop|be&3S your/pro:poss:det|your bear/n|bear sleeping/part|sleep-PROG in/adv:loc|in there/adv:loc|there ?/?|?
u552	CHI	no/co|no ./.|.
u553	MOT	no/co|no ?/?|?
u554	CHI	I/pro:sub|I put/v|put&ZERO ./.|.
u555	MOT	where/adv:wh|where 're/aux|be&PRES you/pro|you going/part|go-PROG with/prep|with those/pro:dem|those ?/?|?
u556	MOT	hiding/part|hide-PROG them/pro:obj|them on/prep|on you/pro|you ./.|.
u557	MOT	do/mod|do n't/neg|not dump/v|dump them/pro:obj|them in/prep|in your/pro:poss:det|your toybox/n|toy_box ./.|.
u558	MOT	we/pro:sub|we 'll/mod|will never/adv|never find/v|find them/pro:obj|them ./.|.
u559	MOT	do/mod|do n't/neg|not put/v|put&ZERO them/pro:obj|them in/adv:loc|in there/adv:loc|there ./.|.
u560	MOT	bring/v|bring them/pro:obj|them out/adv:loc|out here/adv:loc|here ./.|.
u561	MOT	please/co|please ./.|.
u562	INV	can/mod|can you/pro|you show/v|show us/pro:obj|us what/pro:wh|what 's/v:cop|be&3S in/adv:loc|in there/adv:loc|there ?/?|?
u563	CHI	
u564	MOT	I/pro:sub|I lose/v|lose more/qn|more things/n|thing-PL in/prep|in her/pro:poss:det|her toybox/n|toy_box ./.|.
u565	MOT	what/pro:wh|what are/aux|be&PRES they/pro:sub|they doing/part|do-PROG ?/?|?
u566	CHI	sleeping/part|sleep-PROG ./.|.
u567	MOT	sleeping/part|sleep-PROG ?/?|?
u568	MOT	do/mod|do we/pro:sub|we hafta/mod:aux|hafta to/inf|to be/v:cop|be quiet/n|quiet ?/?|?
u569	CHI	no/co|no ./.|.
u570	MOT	no/co|no ?/?|?
u571	MOT	oh/co|oh that/pro:dem|that 's/v:cop|be&3S good/adj|good ./.|.
u572	CHI	
u573	CHI	no/co|no do/mod|do n't/neg|not no/co|no -/-|-
u574	MOT	
u575	CHI	do/mod|do n't/neg|not take/v|take do/mod|do n't/neg|not take/v|take them/pro:obj|them them/pro:obj|them out/n|out ./.|.
u576	MOT	oh/co|oh I/pro:sub|I ca/mod|can n't/neg|not take/v|take them/pro:obj|them out/n|out ?/?|?
u577	CHI	yeah/co|yeah ./.|.
u578	CHI	leave/v|leave them/pro:obj|them ./.|.
u579	CHI	do/mod|do n't/neg|not touch/v|touch ./.|.
u580	MOT	sorry/co|sorry ./.|.
u581	INV	the/det|the door/n|door ./.|.
u582	MOT	she/pro:sub|she 's/aux|be&3S hiding/part|hide-PROG them/pro:obj|them on/prep|on you/pro|you ./.|.
u583	MOT	ca/mod|can n't/neg|not have/v|have those/pro:dem|those back/adv:loc|back ./.|.
u584	MOT	do/mod|do you/pro|you like/v|like those/det|those animals/n|animal-PL ?/?|?
u585	CHI	yeah/co|yeah ./.|.
u586	CHI	they/pro:sub|they wan/v|want na/inf|to go/v|go sleep/n|sleep ./.|.
u587	MOT	do/mod|do we/pro:sub|we hafta/mod:aux|hafta be/v:cop|be quiet/adj|quiet ?/?|?
u588	CHI	yeah/co|yeah ./.|.
u589	MOT	what/pro:wh|what do/mod|do you/pro|you do/v|do ?/?|?
u590	CHI	animals/n|animal-PL sleeping/part|sleep-PROG ./.|.
u591	MOT	can/mod|can you/pro|you go/v|go -/-|-
u592	MOT	shh/co|shh ?/?|?
u593	CHI	
u594	CHI	they/pro:sub|they gon/part|go-PROG na/inf|to go/v|go sleep/n|sleep ./.|.
u595	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO table/n|table alright/co|alright ?/?|?
u596	MOT	alright/co|alright ./.|.
u597	CHI	I/pro:sub|I mana/n|mana I/pro:sub|I 'm/v:cop|be&1S mana/n|mana put/v|put&ZERO I/pro:sub|I put/v|put&ZERO ./.|.
u598	CHI	
u599	CHI	oh/co|oh ./.|.
u600	CHI	I/pro:sub|I 'm/aux|be&1S taking/part|take-PROG them/pro:obj|them out/n|out ./.|.
u601	MOT	can/mod|can I/pro:sub|I take/v|take them/pro:obj|them out/n|out ?/?|?
u602	CHI	no/co|no ./.|.
u603	MOT	no/co|no ?/?|?
u604	CHI	do/mod|do n't/neg|not touch/v|touch ./.|.
u605	CHI	hey/co|hey no/co|no -/-|-
u606	CHI	hey/co|hey no/co|no do/mod|do n't/neg|not take/v|take them/pro:obj|them ./.|.
u607	MOT	okay/co|okay I/pro:sub|I wo/mod|will n't/neg|not touch/v|touch them/pro:obj|them ./.|.
u608	CHI	let/v|let&ZERO me/pro:obj|me take/v|take them/pro:obj|them out/n|out ./.|.
u609	CHI	do/mod|do n't/neg|not ./.|.
u610	MOT	can/mod|can I/pro:sub|I touch/v|touch this/det|this one/pro:indef|one ?/?|?
u611	CHI	no/co|no ./.|.
u612	MOT	how/adv:wh|how about/prep|about this/det|this one/pro:indef|one ?/?|?
u613	CHI	no/co|no ./.|.
u614	CHI	how/adv:wh|how about/prep|about this/det|this one/pro:indef|one huh/co|huh ?/?|?
u615	CHI	there/adv:loc|there the/det|the ./.|.
u616	CHI	she/pro:sub|she sleeping/part|sleep-PROG ./.|.
u617	MOT	okay/co|okay ./.|.
u618	CHI	dododo/bab|dododo they/pro:sub|they 're/aux|be&PRES going/part|go-PROG sleep/n|sleep ./.|.
u619	CHI	
u620	MOT	what/pro:wh|what ?/?|?
u621	CHI	go/v|go go/v|go go/v|go go/v|go go/v|go to/inf|to sleep/v|sleep alright/adj|alright ?/?|?
u622	MOT	alright/co|alright ./.|.
u623	CHI	do/mod|do n't/neg|not touch/v|touch ./.|.
u624	CHI	hey/co|hey no/co|no -/-|-
u625	CHI	stop/v|stop ./.|.
u626	CHI	do/mod|do n't/neg|not touch/v|touch ./.|.
u627	MOT	can/mod|can I/pro:sub|I touch/v|touch this/pro:dem|this ?/?|?
u628	CHI	no/co|no ./.|.
u629	CHI	huh/co|huh -/-|-
u630	CHI	this/det|this one/pro:indef|one 's/v:cop|be&3S a/det|a a/det|a animal/n|animal and/coord|and a/det|a thing/n|thing ./.|.
u631	CHI	hey/co|hey got/v|get&PAST look/n|look ./.|.
u632	MOT	can/mod|can you/pro|you fix/v|fix it/pro|it ?/?|?
u633	CHI	I/pro:sub|I ca/mod|can n't/neg|not ./.|.
u634	CHI	how/adv:wh|how about/prep|about this/det|this one/pro:indef|one ?/?|?
u635	CHI	huh/co|huh how/adv:wh|how about/prep|about this/det|this one/pro:indef|one ?/?|?
u636	MOT	I/pro:sub|I do/mod|do n't/neg|not think/v|think that/pro:dem|that 'll/mod|will fit/v|fit&ZERO in/adv:loc|in there/adv:loc|there ./.|.
u637	MOT	oh/co|oh I/pro:sub|I did/mod|do&PAST n't/neg|not see/v|see him/pro:obj|him ./.|.
u638	MOT	who/pro:wh|who 's/v:cop|be&3S this/pro:dem|this ?/?|?
u639	CHI	a/det|a man/n|man ./.|.
u640	MOT	a/det|a man/n|man ./.|.
u641	CHI	oh/co|oh look/v|look like/prep|like Gracie/n:prop|Gracie ./.|.
u642	CHI	this/det|this go/v|go in/adv:loc|in ./.|.
u643	CHI	this/det|this go/v|go ./.|.
u644	CHI	gon/part|go-PROG na/inf|to let/v|let&ZERO me/pro:obj|me put/v|put&ZERO this/det|this one/pro:indef|one ./.|.
u645	CHI	messy/adj|mess&dn-Y one/pro:indef|one ./.|.
u646	CHI	let/v|let&ZERO me/pro:obj|me put/v|put&ZERO a/det|a other/qn|other red/adj|red one/pro:indef|one in/adv:loc|in ./.|.
u647	MOT	okay/co|okay which/det:wh|which one/pro:indef|one is/v:cop|be&3S the/det|the red/adj|red one/pro:indef|one ?/?|?
u648	CHI	no/co|no this/pro:dem|this a/det|a blue/n|blue ./.|.
u649	CHI	this/pro:dem|this a/det|a color/n|color ./.|.
u650	CHI	let/v|let&ZERO me/pro:obj|me get/v|get another/qn|another one/pro:indef|one ./.|.
u651	MOT	there/adv:loc|there 's/v:cop|be&3S no/qn|no more/qn|more over/prep|over there/adv:loc|there ./.|.
u652	CHI	yeah/co|yeah are/v:cop|be&PRES alright/co|alright here/adv:loc|here ./.|.
u653	MOT	okay/co|okay ./.|.
u654	CHI	right/adj|right ?/?|?
u655	CHI	do/mod|do n't/neg|not stop/v|stop ./.|.
u656	CHI	right/adj|right ?/?|?
u657	CHI	no/co|no ./.|.
u658	MOT	
u659	CHI	oh/co|oh my/pro:poss:det|my leg/n|leg ./.|.
u660	MOT	what/pro:wh|what happened/v|happen-PAST to/prep|to it/pro|it ?/?|?
u661	MOT	noisy/adj|noise&dn-Y ./.|.
