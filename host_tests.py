import os, host, unittest, tempfile, json, uuid
from host import EditInvalid

class HostTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, host.app.config['DATABASE'] = tempfile.mkstemp()
        host.app.config['TESTING'] = True
        self.app = host.app.test_client()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(host.app.config['DATABASE'])


    def test_corpora(self):
        """Tests the json_corpora function. 
        """
        rv = self.app.get('/api/corpora/')
        assert 'valian' in rv.data

    def test_files(self):
        """Tests the json_files function. 
        """
        rvs = self.access('/api/corpora/')
        assert '17a.xml' in rvs['valian']

    def access(self, base_url):
        """Retrieves all subfolder names from a given url call to host.
        Returns the calls to those subfolders together.
        """
        rvs = {}
        for big_unit in json.loads((self.app.get(base_url)).data):
            this_rv = self.app.get(base_url + '{}/'.format(big_unit)).data
            rvs[big_unit] = this_rv
        return rvs


    def correct(self, case):
        """Takes a dict describing the entries into the form at /correction.
        If format is json, applies this to the url to make /correction?format=json
        Returns the response value from post_correction
        """
        url = "/correction"
        return self.app.post(url,
                             data=case)

    def correction(self, pk="", editlist=[]):
        """Constructs a dict describing an editing session.
        Contains a primary key, and a json representation
        of a list of edit (dict)s.
        """
        jedits = json.dumps(editlist)
        return dict(pk=pk,
                    edit=jedits)

    def edit(self, type="", target=[], replacement=[]):
        """Constructs a dict describing a single 'edit'.
        An edit has a type, a list of target word uuids,
        and a list of words associated with the edit.
        """
        return dict(type=type,
                    target=target,
                    replacement=replacement)

    def test_correct(self):
        """A tester for /correction or host.post_correction.
        The local variables v** represent valid inputs.
        The local variables iv** represent invalid inputs.
        
        cases is a list of 3-tuples. Each has a descriptor, 
        a message, and an editlist.
        The descriptor only exists for identification and easy reading.
        The message should exist somewhere in the output.
        The editlist is a list of edits that constitute the input.
        
        For each case, this tester runs check_output with format json.
        This tests that the given message is found in the correct place in
        the response value for the given editlist.
        """
        # valid inputs
        vpk = "606"
        vtype = 'sub'
        vtarget = self.good_words()
        vwords = [{"word": "cool",
                   "suffix": [],
                   "prefix": [],
                   "subPos": [],
                   "pos": "adj",
                   "fusion": [],
                   "stem": "cool",
                   "uuid": uuid.uuid4().hex[:8]},
                  {"word": "ship",
                   "suffix": [],
                   "prefix": [],
                   "subPos": [],
                   "pos": "n",
                   "fusion": [],
                   "stem": "ship",
                   "uuid": uuid.uuid4().hex[:8]}
                  ]

        # invalid inputs
        ivs = "badtext"
        ivd = "-1"
        ivwords = [{"word": "cool",
                    "suffix": [],
                    "prefix": [],
                    "subPos": [],
                    "pos": "adj",
                    "fusion": [],
                    "stem": "cool",
                    "uuid": uuid.uuid4().hex[:8]},
                   {"drow": "graey",
                    "midfix": [],
                    "superPos": [],
                    "cheap trick": [],
                    "flower": "greet"}
                   ]
        ivwords2 = [{"word": "",
                    "suffix": [],
                    "prefix": [],
                    "subPos": [],
                    "pos": "",
                    "fusion": [],
                    "stem": "",
                    "uuid": ""}]


        cases = [('bad_edit', 'edit missing key',
                  self.correction(pk=vpk,
                                  editlist=[{'type': vtype,
                                             'target': vtarget}
                                            ]
                                  )
                  ),
                 ('bad_pk', 'primary',
                  self.correction(pk=ivd,
                                  editlist=[self.edit(type=vtype,
                                                      target=vtarget,
                                                      replacement=vwords)
                                            ]
                                  )
                  ),
                 ('bad_type', 'not defined',
                  self.correction(pk=vpk,
                                  editlist=[self.edit(type=ivs,
                                                      target=vtarget,
                                                      replacement=vwords)
                                            ]
                                  )
                  ),
                 ('no_target', 'not targeted',
                  self.correction(pk=vpk,
                                  editlist=[self.edit(type=vtype,
                                                      target=[],
                                                      replacement=vwords)
                                            ]
                                  )
                  ),
                 ('bad_target', 'target not found',
                  self.correction(pk=vpk,
                                  editlist=[self.edit(type=vtype,
                                                      target=ivwords,
                                                      replacement=vwords)
                                            ]
                                  )
                  ),
                 ('bad_words', 'word missing key',
                  self.correction(pk=vpk,
                                  editlist=[self.edit(type=vtype,
                                                      target=vtarget,
                                                      replacement=ivwords)
                                            ]
                                  )
                  ),
                 ('bad_words2', 'blank',
                  self.correction(pk=vpk,
                                  editlist=[self.edit(type=vtype,
                                                      target=vtarget,
                                                      replacement=ivwords2)
                                            ]
                                  )
                  ),
                 ('multi_fail', 'not defined',
                  self.correction(pk=vpk,
                                  editlist=[self.edit(type=ivs,
                                                      target=vtarget,
                                                      replacement=vwords),
                                            self.edit(type=vtype,
                                                      target=[],
                                                      replacement=vwords),
                                            self.edit(type=vtype,
                                                      target=ivwords,
                                                      replacement=vwords),
                                            self.edit(type=vtype,
                                                      target=vtarget,
                                                      replacement=ivwords)
                                            ]
                                  )
                  )
                 ]

        success_case = ('success', 'success',
                        self.correction(pk=vpk,
                                        editlist=[self.edit(type=vtype,
                                                            target=vtarget,
                                                            replacement=vwords)
                                                ]
                                        )
                        )

        assert 'redirect' in self.correct(cases[0][2]).data

        self.login()
        for case in cases:
            self.check_output(case)
        assert 'success' in json.loads(self.correct(success_case[2]).data)['status']
        self.logout()

    def check_output(self, corrcase):
        """Takes a 'corrcase' 3-tuple holding a descriptor,
        a message and a correction. Also, frmt = format.
        Retrieves the response from post_correction for the correction.
        If format is json, loads the response and checks for 
        the presence of message in the loaded data's message key.
        Otherwise, just checks the entire data for the message.
        """
        message = corrcase[1]
        rv = self.correct(corrcase[2])
        jrv = json.loads(rv.data)
        '''
        if jrv.get('edited_utt'):
            print jrv['edited_utt']
        print "is {} in {}?".format(message, jrv['message'])
        # '''
        assert message in jrv['message']


    def test_nextUC(self):
        """Tests the next_unchecked method of host.py.
        Takes ages so we don't always test it.
        """
        url = '/correction/next'
        orig = json.loads(self.app.get(url).data)
        # print orig

        bid = orig['message']['id']

        assert json.loads(self.app.get(url).data)['message']['id'] == bid
        assert json.loads(self.app.get(url).data)['message']['id'] == bid
        assert json.loads(self.app.get(url).data)['message']['id'] == bid

        self.login()

        assert json.loads(self.app.get(url).data)['message']['id'] == bid
        assert json.loads(self.app.get(url).data)['message']['id'] == bid + 1

        x = json.loads(self.app.get(url).data)['message']['id']
        if x != bid + 2:
            print "{} is not {} + 2".format(x, bid)

        assert json.loads(self.app.get(url).data)['message']['id'] == bid + 2

        self.logout()


    def test_applyEL(self):
        """Tests the apply_editlist method of host.py
        """
        self.login()

        utt_words = self.good_words()
        type1 = 'sub'
        target1 = utt_words[1:]
        replacement1 = [{"word": "lion",
                         "suffix": [],
                         "prefix": [],
                         "subPos": [],
                         "pos": "n",
                         "fusion": [],
                         "stem": "lion",
                         "uuid": "20666f64"},
                        {"word": "?",
                         "suffix": [],
                         "prefix": [],
                         "subPos": [],
                         "pos": "?",
                         "fusion": [],
                         "stem": "?",
                         "uuid": "f9f9adf7"}]

        type2 = 'ins'
        target2 = [replacement1[0]]
        replacement2 = [{"word": "bold",
                         "suffix": [],
                         "prefix": [],
                         "subPos": [],
                         "pos": "adj",
                         "fusion": [],
                         "stem": "bold",
                         "uuid": "ac421a81"}]

        editlist = [self.edit(type1, target1, replacement1),
                    self.edit(type2, target2, replacement2)]

        corrected = host.apply_editlist(editlist, utt_words)

        line = ""
        for cw in corrected:
            line += cw['word'] + " "
        print line
        assert ' lion bold ? ' in line


        type3 = 'sub'
        target3 = [{"word": "blod",
                    "suffix": [],
                    "prefix": [],
                    "subPos": [],
                    "pos": "adj",
                    "fusion": [],
                    "stem": "bold",
                    "uuid": "ac421a81"}]
        replacement3 = [{"word": "bold",
                         "suffix": [],
                         "prefix": [],
                         "subPos": [],
                         "pos": "adj",
                         "fusion": [],
                         "stem": "bold",
                         "uuid": "2fec9e9d"}]

        el2 = [self.edit(type3, target3, replacement3)]

        try:
            corrected2 = host.apply_editlist(el2, corrected)
        except EditInvalid as eiv:
            assert 'target not found' in eiv.message

        self.logout()

# '''
    def good_words(self):
        """Pulls an example of a word dict list from the database.
        """
        return json.loads(self.app.get('/api/606').data)[0]['words']

    def login(self):
        """Logs the session in. For use in testing.
        """
        self.app.post('/login', data=dict(username='admin', password='vvv'))

    def logout(self):
        """Logs the session out. For use in testing.
        """
        self.app.get('/logout')


if __name__ == '__main__':
    unittest.main()
