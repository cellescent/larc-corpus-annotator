(ns corpus-app.db
  (:require [schema.core :as s
             :include-macros true]))

(s/set-fn-validation! true)

(s/def Word
  {:uuid s/Str
   :word s/Str
   :prefix (s/maybe [s/Str])
   :pos s/Str
   :subPos (s/maybe [s/Str])
   :stem s/Str
   :suffix (s/maybe [s/Str])
   :fusion (s/maybe [s/Str])})

(s/def Utterance {:words [Word]
                  :filename s/Str
                  :corpusname s/Str
                  :uid s/Int
                  :file_id s/Int
                  :id s/Int
                  :speaker s/Str})

(defn gen-uuid []
  (-> (js/Math.random)
    (+ 0.5)
    (* (js/Math.pow 2 32))
    (js/Math.floor)
    (.toString 16)))

(defn empty-editor [utterance]
  {:utterance utterance
   :mode :edit
   :edit-list []
   :pending-edit nil})

(defn empty-word []
  {:uuid (gen-uuid)
   :word ""
   :prefix []
   :pos ""
   :subPos []
   :stem ""
   :suffix []
   :fusion []})

(def head
  {:uuid "head"
   :word ""
   :prefix []
   :pos ""
   :subPos []
   :stem ""
   :suffix []
   :fusion []})

(s/def Edit
  {:type (s/enum :ins :sub)
   :name s/Keyword
   :target [Word]
   :replacement [Word]})

(s/def User
  {:name s/Str
   :id s/Int})

(s/def AppData
  {(s/optional-key :message) (s/maybe s/Any)
   :state (s/enum :empty :logged-out :init-loading :loading :loaded)
   :user (s/maybe User)
   :history [{:timestamp js/Object :utterance Utterance}]
   :preview (s/maybe Utterance)
   :editor {:utterance (s/maybe Utterance)
            :edit-list [Edit]
            :pending-edit (s/maybe Edit)
            :mode (s/enum :edit :insert :delete :combine :split)}})

(def default-db
  {:editor (empty-editor nil)
   :state :empty
   :preview nil
   :history ()
   :user nil})

(s/def ServerResponse
  {:status s/Str
   :message s/Any
   (s/optional-key :user) User})

(s/validate AppData default-db)
