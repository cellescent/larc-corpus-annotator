(ns corpus-app.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]
              [corpus-app.edits :as edits]))

(re-frame/register-sub
  :edit-mode
  (fn [db _]
    (reaction
      (get-in @db [:editor :mode]))))

(re-frame/register-sub
  :edit-list
  (fn [db _]
    (reaction
      (get-in @db [:editor :edit-list]))))

(re-frame/register-sub
  :pending-edit
  (fn [db _]
    (reaction
      (get-in @db [:editor :pending-edit]))))

(re-frame/register-sub
  :edit-target
  (fn [db _]
    (reaction
      (get-in @db [:editor :scratch-edit :target]))))

(re-frame/register-sub
  :selected-utterance
  (fn [db _]
    (reaction
      (let [utterance (get-in @db [:editor :utterance])
            pending (get-in @db [:editor :pending-edit])
            edit-list (get-in @db [:editor :edit-list])
            edit-list (if pending (conj edit-list pending) edit-list)]
        (update utterance :words
          (partial edits/apply-edits edit-list))))))

(re-frame/register-sub
  :message
  (fn [db _]
    (reaction (:message @db))))

(re-frame/register-sub
  :loading-state
  (fn [db] (reaction (:state @db))))

(re-frame/register-sub
  :user
  (fn [db _]
    (reaction (:user @db))))

(re-frame/register-sub
  :history
  (fn [db] (-> @db :history reaction)))

(re-frame/register-sub
  :preview
  (fn [db] (-> @db :preview reaction)))
