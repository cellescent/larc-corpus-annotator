(ns corpus-app.handlers
  (:import goog.History)
  (:require [re-frame.core :as re-frame]
            [corpus-app.db :as db]
            [corpus-app.edits :as edits]
            [ajax.core :refer [GET POST]]
            [schema.core :as s]
            [clojure.string :as string]
            [goog.history.Html5History]
            [secretary.core :as secretary]))

;; note that this does not use any middleware. this is so schema failures don't
;; lead to infinite loops.
(re-frame/register-handler
  :show-message
  (fn [db [_ title body]]
    (assoc db :message {:title title :body body})))

(defn check-and-throw
  "throw an exception if db doesn't match the schema."
  [a-schema db]
  (when-let [problems  (s/check db/AppData db)]
    (re-frame/dispatch [:show-message "schema check failed:" (str problems)])))

(def check-schema-mw
  (re-frame/after (partial check-and-throw db/AppData)))

(def middleware [check-schema-mw ;; ensure the schema is still valid
                 ;; (when ^boolean js/goog.DEBUG re-frame/debug) ;; look in your browser console
                 re-frame/trim-v]) ;; remove event id from event vec

(defn report-server-error
  [{:keys [status status-text failure]}]
  (re-frame/dispatch [:show-message
                      (str " error: " status " " (name failure))
                      status-text]))

(defn parse-data
  "Parse server response json string into cljs data"
  [data]
  (-> data
    (js/JSON.parse)
    (js->clj :keywordize-keys true)))

(defn handler [fun]
  (fn [data]
    (let [parsed (parse-data data)
          diff (s/check db/ServerResponse parsed)]
      (if diff
        (re-frame/dispatch [:show-message "invalid server response"
                            (str [(str diff) parsed])])
        (fun parsed)))))

(re-frame/register-handler
  :initialize-db
  middleware
  (fn [] db/default-db))

(re-frame/register-handler
  :select-utterance
  middleware
  (fn [db [utterance editlist]]
    (let [{:keys [id uid corpusname filename]} utterance]
      (.setToken (goog.history.Html5History.)
        (string/join "/" [id corpusname filename (str "u" uid)])))
    (assoc db
      :preview nil
      :editor (assoc (db/empty-editor utterance) :edit-list
                (or editlist []))
      :state :loaded)))

(defn push-pending-edit
  "Returns the db with the pending edit conjed onto the edit-list"
  [db]
  (let [edit (get-in db [:editor :pending-edit])]
    (if (edits/content? edit)
      (update-in db [:editor :edit-list] conj edit)
      db)))

(re-frame/register-handler
  :select-tool
  middleware
  (fn [db [mode]]
    (-> db
      push-pending-edit
      (update :editor merge {:mode mode
                             :pending-edit nil}))))
(re-frame/register-handler
  :select-word
  middleware
  (fn [db [word]]
    (let [mode (get-in db [:editor :mode])
          repl (condp = mode
                 :edit [(assoc word :uuid (db/gen-uuid))]
                 :split [(assoc word :uuid (db/gen-uuid)) (db/empty-word)])]
      (-> db
        push-pending-edit
        (assoc-in [:editor :pending-edit]
          {:type :sub
           :name mode
           :target [word]
           :replacement repl})))))

(re-frame/register-handler
  :select-slot
  middleware
  (fn [db [word]]
    (-> db
      (assoc-in [:editor :pending-edit]
        {:type :ins
         :name :insert
         :target [word]
         :replacement [(db/empty-word)]}))))

(re-frame/register-handler
  :add-target
  middleware
  (fn [db [word side]]
    (-> db
      (update-in [:editor :pending-edit :target]
        #(condp = side
           :left (into [] (cons word %))
           :right (concat % [word])))
      (update-in [:editor :pending-edit]
        #(merge {:type :sub
                 :name :combine
                 :replacement [(assoc word :uuid (db/gen-uuid))]}
           %)))))

(re-frame/register-handler
  :edit-word
  middleware
  (fn [db [n attr val]]
    (assoc-in db [:editor :pending-edit :replacement n attr] val)))

(re-frame/register-handler
  :delete-word
  middleware
  (fn [db [target]]
    (update-in db [:editor :edit-list]
      #(conj % {:type :sub
                :name :delete
                :target [target]
                :replacement []})))) ;; TODO - change this to replacement

(re-frame/register-handler
  :discard-edits
  middleware
  (fn [db []]
    (update db :editor merge
      {:edit-list []
       :pending-edit nil})))

(re-frame/register-handler
  :pop-edit
  middleware
  (fn [db []]
    (update db :editor
      (fn [m]
        (if (:pending-edit m)
          (assoc m :pending-edit nil)
          (update m :edit-list pop))))))

(re-frame/register-handler
  :submit-correction
  middleware
  (fn [db []]
    (let [db (push-pending-edit db)
          {:keys [words id] :as utt} (get-in db [:editor :utterance])
          edit-list (-> db :editor :edit-list)]
      (POST "/correction"
          {:format :raw
           :error-handler report-server-error
           :handler (handler
                      (fn [{status :status
                            edited_utt :message :as data}]
                        (let [success? (= status "success")
                              edits-eq? (= edited_utt
                                          (edits/apply-edits edit-list words))
                              fmt #(string/join "/" [(:word %)])]
                          (cond
                            (and success? edits-eq?)
                            (do
                              (re-frame/dispatch [:append-history {:timestamp (js/Date.)
                                                                   :utterance utt}])
                              (re-frame/dispatch [:next-correction]))

                            (not success?)
                            (re-frame/dispatch [:show-message "Error" (str data)])

                            (not edits-eq?)
                            (re-frame/dispatch
                              [:show-message
                               "client/server application mismatch"
                               [:table
                                [:li "server: "
                                 (string/join " "
                                   (map fmt edited_utt))]
                                [:li "client: "
                                 (string/join " "
                                   (map fmt (edits/apply-edits edit-list words)))]]])))))
           :params {:pk id
                    :edit (-> edit-list (or []) clj->js js/JSON.stringify)}}))
    (dissoc db :message)))

(re-frame/register-handler
  :append-history
  middleware
  (fn [db [item]]
    (update db :history conj item)))

(re-frame/register-handler
  :load-utterance
  middleware
  (fn [db [id]]
    (GET (str "/api/utterance/" id)
        {:error-handler report-server-error
         :handler (handler
                    (fn [{:keys [status message user]}]
                      (let [{:keys [utterance edits]} message
                            editlist (-> edits last :editlist)
                            editlist (into [] (for [e editlist]
                                                (-> e (update :type keyword)
                                                  (update :name keyword))))]
                        (re-frame/dispatch [:set-user user])
                        (re-frame/dispatch [:select-utterance utterance editlist]))))})
    (-> db
      (assoc :state (if (-> db :editor :utterance nil?)
                      :init-loading
                      :loading))
      (dissoc :message))))

(defn navigate
  [direction db]
  {:pre [(get #{:next :prev} direction)]}
  (GET "/api/utterance/neighbors"
      {:params {:id (-> db :editor :utterance :id)}
       :format :raw
       :error-handler report-server-error
       :handler (handler
                  (fn [{:keys [message]}]
                    (re-frame/dispatch [:load-utterance (get message direction)])))})
  (assoc db :state :loading))

(re-frame/register-handler
  :next-utterance
  middleware
  (partial navigate :next))

(re-frame/register-handler
  :prev-utterance
  middleware
  (partial navigate :prev))

(re-frame/register-handler
  :next-correction
  middleware
  (fn [db []]
    (GET "/correction/next"
        {:error-handler report-server-error
         :handler (handler
                    (fn [{:keys [status message user]}]
                      (re-frame/dispatch [:set-user user])
                      (re-frame/dispatch [:select-utterance message])))})
    (-> db
      (assoc :state (if (-> db :editor :utterance nil?)
                      :init-loading
                      :loading))
      (dissoc :message))))

(re-frame/register-handler
  :login
  middleware
  (fn [db [username password]]
    (POST "/api-login"
        {:params {:username username :password password}
         :format :raw
         :error-handler report-server-error
         :handler (handler
                    (fn [{:keys [status user message]}]
                      (case status
                        "success" (if-let [path (-> js/window .-location .-hash not-empty)]
                                    (secretary/dispatch! path)
                                    (re-frame/dispatch [:next-correction]))
                        "failure" (if (and username password)
                                    (re-frame/dispatch [:show-message "error" (:cause message)])
                                    (re-frame/dispatch [:logout])))))})
    db))

(re-frame/register-handler
  :logout
  middleware
  (fn [db [user]]
    (assoc db :state :logged-out)))

(re-frame/register-handler
  :loaded
  middleware
  (fn [db [user]]
    (assoc db :state :loaded)))

(re-frame/register-handler
  :set-user
  middleware
  (fn [db [user]]
    (assoc db :user user)))

(re-frame/register-handler
  :sample-utterance
  middleware
  (fn [db [utterance]]
    (assoc db :preview utterance)))

(re-frame/register-handler
  :cancel-sample
  middleware
  (fn [db [utterance]]
    (assoc db :preview nil)))
