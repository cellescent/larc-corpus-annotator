(ns corpus-app.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [devtools.core :as devtools]
            [corpus-app.handlers]
            [corpus-app.subs]
            [corpus-app.routes :as routes]
            [corpus-app.views :as views]
            [corpus-app.config :as config]
            [ajax.core :refer [GET POST]]))


(defn dev-setup []
  (when config/debug?
    (println "dev mode")
    (devtools/install!)))

(defn mount-root []
  (reagent/render [views/main-panel]
    (.getElementById js/document "app"))
  (reagent/render [views/navbar]
    (.getElementById js/document "navbar")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root)
  (re-frame/dispatch [:login]))
