(ns corpus-app.views
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]
            [clojure.string :as string]
            [corpus-app.db :as db]
            [corpus-app.utils :refer [row col glyph] :as utils]
            [corpus-app.routes :as routes]
            [secretary.core :as secretary]))

(defn mode-icon [mode]
  (case mode
    :edit "edit"
    :insert "plus"
    :delete "trash"
    :split "scissors"
    :combine "resize-small"))

(defn tool-palette []
  (let [edit-mode (re-frame/subscribe [:edit-mode])]
    (fn []
      (into [:div {:class-name "tool-palette"}]
        (for [mode [:edit :combine :split :delete :insert]
              :let [selected (= mode @edit-mode)]]
          [:div
           [:button {:key mode
                     :title (name mode)
                     :class-name (str "btn btn-sm " (when selected "btn-primary"))
                     :on-click #(re-frame/dispatch [:select-tool mode])}
            [glyph (mode-icon mode)]]])))))

(def delims {:prefix "#"
             :subPos ":"
             :stem "|"
             :suffix "-"
             :fusion "&"})

(defn headers [attrs]
  (into [:tr]
    (for [attr attrs]
      [:th (name attr)
       (when-let [d (get delims attr)] (str " " d))])))

(defn compound-attr? [attr]
  (-> attr #{:subPos :prefix :suffix :fusion} boolean))

(defn parse-compound [delim st]
  (if (re-find (js/RegExp (str delim "\\s*$")) st)
    (conj (string/split st delim) "")
    (string/split st delim)))

(defn word-input [n word attr]
  (let [val (attr word)
        compound? (compound-attr? attr)
        delim " "]
    [:input {:type "text"
             :size 10
             :on-change (fn [el]
                          (let [val (-> el .-target .-value)
                                val (if compound? (parse-compound delim val) val)]
                            (re-frame/dispatch [:edit-word n attr val])))
             :value (if compound? (string/join delim val) val)}]))


(defn word-editor []
  (fn []
    (let [pending-edit (re-frame/subscribe [:pending-edit])
          replacement (-> @pending-edit :replacement)
          attrs [:word :prefix :pos :subPos :stem :suffix :fusion]]
      (when (not-empty replacement)
        [:div
         (into
           [:table {:class-name "table"} (headers attrs)]
           (for [[n w] (map vector (range) replacement)]
             [:div
              (into [:tr]
                (for [attr attrs]
                  [:td
                   (word-input n w attr)]))]))]))))

(defn pre-compound
  "(pre-compound [x yy z] ':') -> ':x:yy:z'"
  [delim col]
  (when (not-empty col) (str delim (string/join delim col))))

(defn post-compound
  "(pre-compound [x yy z] ':') -> 'x:yy:z:'"
  [delim col]
  (when (not-empty col) (str (string/join delim col) delim)))

(defn word-view
  "Renders a single word for listing in utterance-editor"
  [{:keys [word pos subPos prefix stem suffix fusion]}]
  [:span
   (let [prefix (post-compound "#" prefix)
         subPos (pre-compound ":" subPos)
         suffix (pre-compound "-" suffix)
         fusion (pre-compound "&" fusion)]
     [:div
      [:div {:class-name "wordform"} word]
      [:div {:class-name "pos-tag"}
       (str
         prefix pos subPos
         "|" stem
         suffix fusion)]])])

(defn target? [edit word]
  (get (->> edit :target (map :uuid) set)
    (:uuid word)))

(defn replacement? [edit word]
  (get (->> edit :replacement (map :uuid) set)
    (:uuid word)))

(defn word-selector []
  (fn []
    (let [utterance (re-frame/subscribe [:selected-utterance])
          pending-edit (re-frame/subscribe [:pending-edit])]
      (into [:div {:class-name "word-selector"}]
        (for [word (->> @utterance :words)
              :let [selected (replacement? @pending-edit word)]]
          [:a {:class-name (if selected "word selected" "word selectable")
               :on-click #(when-not selected (re-frame/dispatch [:select-word word]))}
           (word-view word)])))))

(defn window
  ([coll] (window 1 coll))
  ([n coll]
   (let [pad (repeat n nil)]
     (partition (-> n (* 2) inc) 1
       (concat pad coll pad)))))

(defn word-combiner []
  (fn []
    (let [utterance (re-frame/subscribe [:selected-utterance])
          pending-edit (re-frame/subscribe [:pending-edit])]
      (into [:div {:class-name "word-selector"}]
        (for [[left word right] (->> @utterance :words window)
              :let [selected (replacement? @pending-edit word)
                    side (cond (replacement? @pending-edit left) :right
                               (replacement? @pending-edit right) :left
                               :else nil)]]
          (cond
            (nil? @pending-edit)
            [:a {:class-name "word selectable"
                 :on-click #(re-frame/dispatch [:add-target word :left])}
             (word-view word)]

            selected
            [:span
             {:class-name (str "word selected")}
             (word-view word)]

            side
            [:a
             {:class-name (str "word selectable")
              :on-click #(re-frame/dispatch [:add-target word side])}
             (glyph (get {:left "arrow-right" :right "arrow-left"} side))
             (word-view word)]

            :else
            [:span {:class-name "word"} (word-view word)]))))))

(defn word-deleter []
  (fn []
    (let [utterance (re-frame/subscribe [:selected-utterance])]
      (into [:div {:class-name "word-deleter"}]
        (for [w (->> @utterance :words)]
          [:a {:class-name (str "word selectable deletable")
               :on-click #(re-frame/dispatch [:delete-word w])}
           (word-view w)])))))

(defn slot [word]
  [:a {:class-name "insertion-slot selectable"
       :on-click #(re-frame/dispatch [:select-slot word])}
   (glyph "plus-sign")])

(defn slot-selector []
  (fn []
    (let [utterance (re-frame/subscribe [:selected-utterance])
          edit (re-frame/subscribe [:pending-edit])]
      (into [:div {:class-name "slot-selector"} [slot db/head]]
        (for [w (->> @utterance :words)
              :let [selected? (or (replacement? @edit w) (target? @edit w))]]
          [:span
           [:span {:class-name (str "word inactive"
                                 (when selected? " selected"))}
            (word-view w)]
           (when-not selected? [slot w])])))))

(defmulti show-edit :name)
(defmethod show-edit :delete [edit]
  [:span [:small  "delete "] (-> edit :target first :word)])

(defmethod show-edit :insert [edit]
  (let [target (-> edit :target first :word)
        head? (-> edit :target first :uuid (= "head"))]
    [:span [:small  "insert "]
     (-> edit :replacement first :word)
     (if head?
       [:small " at beginning "]
       [:span
        [:small " after "]
        (-> edit :target first :word)])]))

(defmethod show-edit :edit [edit]
  [:span [:small  "edit "]
   (-> edit :target first :word)
   [:small " -> "]
   (-> edit :replacement first :word)])

(defmethod show-edit :default [edit]
  [:span
   [:small (-> edit :name name str)] " "
   (->> edit :target (map :word) (string/join "+")) " -> "
   (->> edit :replacement (map :word) (string/join "+"))])

(defn edit-view []
  (let [edit-list (re-frame/subscribe [:edit-list])
        pending-edit (re-frame/subscribe [:pending-edit])]
    (fn []
      (let [edits (if @pending-edit (conj @edit-list @pending-edit) @edit-list)]
        [:div
         (for [[n edit] (map vector (range) edits)]
           [:div {:key n} (show-edit edit)])
         (when (not-empty edits)
           [:button {:class-name "btn btn-sm"
                     :on-click #(re-frame/dispatch [:pop-edit])}
            [glyph "repeat"] " " "Undo"])]))))

(defn editor [disabled]
  (let [edit-mode (re-frame/subscribe [:edit-mode])
        edit-list (re-frame/subscribe [:edit-list])
        pending (re-frame/subscribe [:pending-edit])
        utterance (re-frame/subscribe [:selected-utterance])]
    (fn [disabled]
      (let [modified? (not (and (empty? @edit-list) (nil? @pending)))
            {:keys [corpusname filename uid speaker]} @utterance]
        [:div {:class-name (when disabled "disabled")}
         [row
          [row
           [col 2 " "]
           [col 2 speaker]
           [col 2 [:button {:class-name (str "btn "
                                          (if modified? "btn-warning" "btn-success"))
                            :disabled disabled
                            :title "Submit this correction and see the next unchecked utterance"
                            :on-click #(re-frame/dispatch [:submit-correction])}
                   (if modified?
                     "Done"
                     "No Errors")]]
           [col 2
            [:button {:class-name "btn"
                      :disabled disabled
                      :title "See the next unchecked utterance"
                      :on-click #(re-frame/dispatch [:next-correction])}
             "Skip"]]


           #_(when modified?
               [col 2
                [:button {:class-name "btn"
                          :disabled disabled
                          :on-click #(re-frame/dispatch [:discard-edits])}
                 "Discard Edits"]])] ]
         [row
          [:div {:class-name "editor"}
           [col 1 [row [col 12 [tool-palette]]]]
           [col 11

            [row [col 12 (condp = @edit-mode
                           :edit [word-selector]
                           :delete [word-deleter]
                           :split [word-selector]
                           :insert [slot-selector]
                           :combine [word-combiner]
                           [:h2 (str @edit-mode " not implemented")])]]
            [row [col 12 [word-editor]]]]]]]))))

(defn message-view []
  (let [messages (re-frame/subscribe [:message])]
    (fn []
      (when-let [{:keys [title body]} (not-empty @messages)]
        [:div {:class-name "well messages alert alert-danger"}
         [:h3
          (glyph "exclamation-sign exclamation")
          title]
         body]))))

(defn navigator-arrow [direction]
  {:pre [(get #{:left :right} direction)]}
  (let [path (get {:left :prev-utterance
                   :right :next-utterance}
               direction)]
    [:a {:class-name "nav-arrow"
         :title (string/replace (name path) #"-" " ")
         :on-click #(re-frame/dispatch [path])}
     [glyph (str "arrow-" (name direction))] ]))

(defn navbar []
  (let [user (re-frame/subscribe [:user])
        utterance (re-frame/subscribe [:selected-utterance])]
    (fn []
      (when @user
        [:div {:class-name "container"}
         [:div {:class-name "navbar-header"}
          [:a {:class-name "navbar-brand"} (:name @user)]]
         [:ul {:class-name "nav navbar-nav"}
          [:li [navigator-arrow :left]]
          [:li [navigator-arrow :right]]
          [:li]
          [:li [:a {:href "#"}
                (let [{:keys [corpusname filename uid]} @utterance]
                  (string/join " / " [corpusname filename uid]))]]]
         [:ul {:class-name "nav navbar-nav navbar-right"}
          [:li [:a {:href "/logout"} "Logout"]]]]))))

(defn form [fields error-target on-submit]
  (let [data (r/atom (into {} (for [[name type] fields]
                                [name ""])))]
    (fn [fields error-target on-submit]
      [:div
       (let [body (for [field (keys fields)
                        :let [field-name (name field)
                              error? (= error-target field-name)]]
                    [:div {:class-name (str "form-group" (when error? " has-error"))}
                     [:span
                      (when error? (glyph "exclamation-sign"))
                      [:label {:for field-name} (-> field name string/capitalize)]]
                     [:input {:type (fields field) :id field-name :placeholder field-name
                              :class-name "form-control form-control-danger"
                              :value (-> @data field)
                              :on-change #(swap! data assoc field (-> % .-target .-value))}]])]
         (into [] (concat
                    [:form]
                    body
                    [[:button {:type "submit"
                               :on-click #(do (.preventDefault %) (on-submit @data))}
                      "Login"]])))])))

(defn login-form []
  (let [error (re-frame/subscribe [:message])]
    (fn []
      [:div
       [form {:username "text" :password "password"}
        (:body @error)
        #(re-frame/dispatch [:login (:username %) (:password %)])]
       [:a {:href "/new-user"} "Create New Account"]])))

(defn history [loading]
  (let [history (re-frame/subscribe [:history])
        selected-utterance (re-frame/subscribe [:selected-utterance])]
    (fn [loading]
      (when (not-empty @history)
        (into [:div  {:class-name "edit-history"}]
          (for [{:keys [timestamp utterance]} @history
                :let [{:keys [id filename uid]} utterance
                      active (and (not loading) (= id (:id @selected-utterance)))]]
            [:div {:key (str (.getTime timestamp) "-" id)}
             [:a {:on-mouse-over #(re-frame/dispatch [:sample-utterance utterance])
                  :on-mouse-out #(re-frame/dispatch [:cancel-sample])
                  :on-click #(-> utterance routes/utterance-path secretary/dispatch!)}
              [:button {:class-name (str "btn btn-xs " (when active "btn-primary"))}
               (utils/format-timestamp timestamp) " "
               (string/join "/" [(string/replace filename #"\.xml" "") (str "u" uid)])]]]))))))

(defn main-panel []
  (let [state (re-frame/subscribe [:loading-state])
        preview (re-frame/subscribe [:preview])]
    (fn []
      [:div {:class-name "container"}
       (condp get @state
         #{:empty :init-loading} [row [col 12 "loading"]]

         #{:logged-out} [login-form]

         #{:loaded :loading}
         [:div
          [row [message-view]]
          [:div {:class-name (when (= @state :loading) "loading")}
           [col 10
            (if-let [preview @preview]
              [:div {:class-name "utterance-preview"}
               (str (:speaker preview) ": "
                 (string/join " " (map :word (:words preview))))]
              [editor (= @state :loading)])]
           [col 2
            [row [col 12 [edit-view]]]
            [row [col 12 [:hr]]]
            [row [col 12 [history (= @state :loading)]]]]]])])))
