# Corpus-Annotator for LARC #

This repo contains code for setting up and interacting with a database based on corpora of transcribed spontaneous child-adult speech.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Run

  pip install -r requirements.txt

in order to install the required python libraries.

### Development and PostGreSQL ###

Before setting up a database, you need to install PostGreSQL, and create a database (optionally, a custom user and password too).

Add to your environmental variables the following:

CORPUS_APP_DB_NAME = [the name of your database]

CORPUS_APP_DB_USER = [the name of your user; default on installation is 'postgres']

CORPUS_APP_DB_PASSWORD = [the password of your user; default is chosen during installation]