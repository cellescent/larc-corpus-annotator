import bcrypt, os, sys, uuid, json
from glob import glob
from talkbank_parser import MorParser, MorToken

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask.ext.script import Manager, Command
from flask.ext.migrate import Migrate, MigrateCommand

import hashlib
BLOCKSIZE = 65536

from schema import app, db, Corpus, File, Utterance, User, Correction
from host import apply_editlist


migrate = Migrate(app, db)
manager = Manager(app)

parser = MorParser("{http://www.talkbank.org/ns/talkbank}")

def make_user(name, password):
    return User(name=name, hashedword=bcrypt.hashpw(bytes(password), bcrypt.gensalt()))

def make_corpus(name, label):
    return Corpus(name=name, label=label)

def make_file(filepath, corpus, hash):
    return File(name=os.path.basename(filepath),
                path=filepath,
                corpus=corpus,
                hash=hash)

def make_utterance(uid, speaker, jsonwords, file):
    return Utterance(uid=int(uid[1:]),
                     speaker=speaker,
                     words=json.dumps(jsonwords),
                     file=file)

def makehash(filepath):
    hasher = hashlib.sha1()
    with open(filepath, 'rb') as xmlfile:
        buffer = xmlfile.read(BLOCKSIZE)
        while len(buffer) > 0:
            hasher.update(buffer)
            buffer = xmlfile.read(BLOCKSIZE)
    return hasher.hexdigest()


@manager.command
def upload(label, name=None):
    '''upload a corpus by label and (optionally) name'''
    if name is None:
        name = label
    dbcorpus = Corpus.query.filter(Corpus.label == label).one_or_none()
    if dbcorpus is None:
        this_corpus = make_corpus(name=name, label=label)
        db.session.add(this_corpus)
        db.session.commit()
    else:
        this_corpus = dbcorpus
    # ''' # toggle by adding a # to the start of this line
    fpaths = glob('{}/*.xml'.format(label))
    ''' # the line below makes testing fast
    fpaths = glob('{}/10*.xml'.format(label))
    # '''
    for fpath in fpaths:
        addfile(fpath, this_corpus)
        db.session.commit()

def addfile(filepath, corpus):
    fhash = makehash(filepath)
    dbfile = File.query.filter(File.hash == fhash).one_or_none()
    if dbfile is not None:
        print dbfile.name + " already exists in this database"
    else:
        print filepath
        new_file = make_file(filepath, corpus, fhash)
        db.session.add(new_file)
        for uid, speaker, ut in parser.parse(filepath):
            words = []
            for token in ut:
                word = token.to_dict()
                word['uuid'] = uuid.uuid4().hex[:8]
                words.append(word)
                new_ut = make_utterance(uid=uid,
                                        speaker=speaker,
                                        jsonwords=words,
                                        file=new_file)
                db.session.add(new_ut)

def adduser(username, password):
    dbuser = User.query.filter(User.name == username).one_or_none()
    if dbuser is None:
        new_user = make_user(username, password)
        db.session.add(new_user)
        db.session.commit()
    else:
        print username + " already exists in this database"

class Genesis(Command):
    '''populates the database from scratch'''
    def run(self):
        adduser('admin', 'vvv')
        upload(label='tagger-evaluation-docs-dropbox_xml-converted-from-cha',
               name='valian')

    def revert(self):
        User.query.delete()
        Correction.query.delete()
        Utterance.query.delete()
        File.query.delete()
        Corpus.query.delete()
        db.session.commit()

@manager.command
def cleardb():
    '''completely clears the database'''
    Genesis().revert()


class Gold(Command):
    '''Dumps gold txt files of every corpus in the database.'''
    def run(self):
        for corpus in Corpus.query.all():
            self.gold_corpus(corpus)

    def gold_corpus(self, corpus, outdir=None):
        if outdir is None:
            outdir = "corrected-" + corpus.name
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        for file in File.query.filter(File.corpus_id == corpus.id):
            self.gold_file(file, outdir)

    def gold_file(self, file, outdir):
        print 'Now gilding {}'.format(file.name)
        outfile = "{}/{}.txt".format(outdir, file.name)
        with open(outfile, mode='w') as f:
            ordered_utts = Utterance.query.filter(Utterance.file_id == file.id).all()

            for utt in ordered_utts:
                words = json.loads(utt.words)
                corrs = Correction.query.filter(Correction.utterance_id == utt.id,
                                                Correction.accepted == 1).all()
                if len(corrs) >= 1:
                    editlist = json.loads(corrs[0].editlist)
                    words = apply_editlist(editlist, words)

                tokens = self.tokenize(words)
                self.append_to_file(f, utt, tokens)

    def append_to_file(self, f, utterance, tokens):
        f.write("u{}\t{}\t".format(utterance.uid, utterance.speaker))
        f.write(' '.join([token.__repr__() for token in tokens]))
        f.write("\n")

    def tokenize(self, words):
        tokens = []
        for word in words:
            word['sxfx'] = word.pop('fusion')
            word['sfx'] = word.pop('suffix')
            word.pop('uuid')
            tokens.append(MorToken(**word))
        return tokens

@manager.command
def gold(corpus_name, outdir=None):
    '''Dumps gold txts. Takes corpus name, optional output directory name.
    '''
    corpus = Corpus.query.filter(Corpus.name == corpus_name).one_or_none()
    if corpus is None:
        print 'no such corpus in the database'
    else:
        Gold().gold_corpus(corpus, outdir)

manager.add_command('new', Genesis())
manager.add_command('db', MigrateCommand)
manager.add_command('allgold', Gold())

if __name__ == "__main__":
    manager.run()
